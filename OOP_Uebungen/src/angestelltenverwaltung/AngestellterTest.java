package angestelltenverwaltung;

public class AngestellterTest {
	
	public static void main(String[] args) {

		 Angestellter ang1 = new Angestellter("Meier", 4500);
		 Angestellter ang2 = new Angestellter("Petersen", 6000); 
		 Angestellter ang3 = new Angestellter("Schmid", 2000);
		 
		 //Setzen der Attribute
		 //ang1.setName("Meier");
		 //ang1.setGehalt(4500);
		 //ang2.setName("Petersen");
		 //ang2.setGehalt(6000);
		 
		 System.out.println("Name: " + ang1.getName());
		 System.out.println("Gehalt: " + ang1.getGehalt() + " Euro");
		 System.out.println("\nName: " + ang2.getName());
		 System.out.println("Gehalt: " + ang2.getGehalt() + " Euro");
		 System.out.println("\nName: " + ang3.getName());
		 System.out.println("Gehalt: " + ang3.getGehalt() + " Euro");
	}
	 
}
