package angestelltenverwaltung;

public class Angestellter {

	//Atribute
	private String name;
	private double gehalt;
	
	//1. Parameterloser-Konstruktor danach 2+3 Kon.
	public Angestellter () 
	{    }
	
	public Angestellter (String name) {
		this.name = name;
		this.gehalt = 0;
	}

	public Angestellter (String name, double gehalt) {
		this.name = name;
		this.gehalt = gehalt;
	}
	
	
	//Methode 
	public void setName(String name)
	{
		this.name = name;
	}
	
	public String getName ()
	{
		return this.name;
	}
	
	public void setGehalt(double gehalt)
	{
		 this.gehalt = gehalt;
	}
	
	public double getGehalt()
	{
		 return this.gehalt;
	}
}


