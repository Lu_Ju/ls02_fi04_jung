package test;

import LS02_5_01.*;

public class test {
	
		public static void main(String[] args) {
	        Raumschiff klingonen = new Raumschiff("IKS Hegh'ta", 100, 100, 100, 100, 1, 2);
	        Ladung klingonenLadung1 = new Ladung("Ferengi Schneckensaft", 200);
	        Ladung klingonenLadung2 = new Ladung("Bat'leth Klingonen Schwert", 200);
	        klingonen.ladungAufnehmen(klingonenLadung2);
	        klingonen.ladungAufnehmen(klingonenLadung1);
	    
	        Raumschiff romulaner = new Raumschiff("IRW Khazara", 100, 100, 100, 100, 2, 2);
	        Ladung romulanerLadung1 = new Ladung("Borg-Schrott", 5);
	        Ladung romulanerLadung2 = new Ladung("Rote Materie", 2);
	        Ladung romulanerLadung3 = new Ladung("Plasma-Waffe", 50);
	        romulaner.ladungAufnehmen(romulanerLadung1);
	        romulaner.ladungAufnehmen(romulanerLadung2);
	        romulaner.ladungAufnehmen(romulanerLadung3);
	        
	        Raumschiff vulkanier = new Raumschiff("Ni'Var", 80, 80, 100, 50, 0, 5);
	        Ladung vulkanierLadung1 = new Ladung("Forschungssonde", 35);
	        Ladung vulkanierLadung2 = new Ladung("Photonentorpedo", 3);
	        vulkanier.ladungAufnehmen(vulkanierLadung1);
	        vulkanier.ladungAufnehmen(vulkanierLadung2);
	        
	        klingonen.photonentorpedoAbschießen(romulaner);
	        romulaner.phaserkanoneAbschießen(klingonen);
	        vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
	        klingonen.zustandAusgeben();
	        klingonen.ladungsverzeichnisAusgeben();
	        klingonen.photonentorpedoAbschießen(romulaner);
	        klingonen.photonentorpedoAbschießen(romulaner);
	        System.out.println("");
	        klingonen.zustandAusgeben();
	        klingonen.ladungsverzeichnisAusgeben();
	        romulaner.zustandAusgeben();
	        romulaner.ladungsverzeichnisAusgeben();
	        vulkanier.zustandAusgeben();
	        vulkanier.ladungsverzeichnisAusgeben();  
	        
	    }
}