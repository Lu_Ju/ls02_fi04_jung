package LS02_5_01;

import java.util.ArrayList;

/**
 * Blaupause f�r Raumschiff.
 * @author L.J
 * @version 1.0
 */
public class Raumschiff {
	private String schiff;
	private int energieversorgung;
	private int schutzschilde;
	private int lebenserhaltungssysteme;
	private int huelle;
	private int photonentorpedos;
	private int reparaturAndroiden;	
	private ArrayList<String> broadcastKommunikator;
	private ArrayList<Ladung> ladungsverzeichnis;
	
	/**
	 * Standart Konstruktor - erstellt die Arrays.
	 */
	public Raumschiff() {
		this.broadcastKommunikator = new ArrayList<String>();
		this.ladungsverzeichnis = new ArrayList<Ladung>();
	}
	
	/**
	 * Voller Konstruktor setzt alle Attribute auf die Werte - ruft standart Konstruktor auf.
	 * 
	 * @param schiff - Der Schiffsname.
	 * @param energieversorgung - Die Energieversorgung (%).
	 * @param schutzschilde - Das schutzschilde (%).
	 * @param lebenserhaltungssysteme - Das Lebenserhaltungssystem (%).
	 * @param huelle - Die H�lle (%).
	 * @param photonentorpedos - Die Photonentorpedo Anzahl.
	 * @param reparaturAndroiden - Die Reparaturandroiden Anzahl.
	 */
	public Raumschiff (String schiff, int energieversorgung, int schutzschilde, int lebenserhaltungssysteme, int huelle, int photonentorpedos, int reparaturAndroiden) {
		this();
		this.schiff = schiff;
		this.energieversorgung = energieversorgung;
		this.schutzschilde = schutzschilde;
		this.lebenserhaltungssysteme = lebenserhaltungssysteme;
		this.huelle = huelle;
		this.photonentorpedos = photonentorpedos;
		this.reparaturAndroiden = reparaturAndroiden;		
	}	
	
	/**
	 * Setzt den Namen des Schiffes.
	 * @param schiff - Neuer Name des Schiffes.
	 */
	public void setSchiff(String schiff) {
		this.schiff = schiff;
	}
	
	/**
	 * Gibt den Namen des Schiffes.
	 * @return schiff - Der Name des Schiff.
	 */
	public String getSchiff() {
		return this.schiff;
	}
	
	/**
	 * Setzt die Energieversorgung.
	 * @param energieversorgung - Neuer Wert der Energieversorgung.
	 */
	public void setEnergieversorgung(int energieversorgung) {
		this.energieversorgung = energieversorgung;
	}
	
	/**
	 * Gibt die Energieverorgung.
	 * @return Wert der Energieversorgung.
	 */
	public int getEnergieversorgung() {
		return this.energieversorgung;
	}
	
	/**
	 * Setzt das Schutzschild.
	 * @param schutzschilde - Neuer Wert des Schutzschildes.
	 */
	public void setSchutzschilde(int schutzschilde) {
		this.schutzschilde = schutzschilde;
	}
	
	/**
	 * Gibt das Schutzschild.
	 * @return Wert des Schutzschildes.
	 */
	public int getSchutzschilde() {
		return this.schutzschilde;
	}
	
	/**
	 * Setzt das Lebenserhaltungssystem.
	 * @param lebenserhaltungssysteme - Neuer Wert des Lebenserhaltungssystem.
	 */
	public void setLebenserhaltungsSysteme(int lebenserhaltungssysteme) {
		this.lebenserhaltungssysteme = lebenserhaltungssysteme;
	}
	
	/**
	 * Gibt das Lebenserhaltungssystem.
	 * @return Wert des Lebenserhaltungssystem.
	 */
	public int getLebenserhaltungsSysteme() {
		return this.lebenserhaltungssysteme;
	}
	
	/**
	 * Setzt die H�lle.
	 * @param huelle - Neuer Wert der H�lle.
	 */
	public void setHuelle (int huelle) {
		this.huelle = huelle;
	}
	
	/**
	 * Gibt die H�lle.
	 * @return Wert der H�lle.
	 */
	public int getHuelle() {
		return this.huelle;
	}
	
	/**
	 * Setzt die Photonentorpedos.
	 * @param photonentorpedos - Anzahl der Photonentorpedos.
	 */
	public void setPhotonentorpedos(int photonentorpedos) {
		this.photonentorpedos = photonentorpedos;
	}
	
	/**
	 * Gibt die Photonentorpedos.
	 * @return Anzahl der Torpedos.
	 */
	public int getPhotonentorpedos() {
		return this.photonentorpedos;
	}
	
	/**
	 * Setzt die Reparaturandroiden.
	 * @param reparaturAndroiden  - Anzahl der Reparaturandroiden.
	 */
	public void setReparaturAndroiden(int reparaturAndroiden) {
		this.reparaturAndroiden = reparaturAndroiden;
	}
	
	/**
	 * Gibt die Reparaturandroiden.
	 * @return Anzahl der Reparaturandroiden. 
	 */
	public int getReparaturAndroiden() {
		return this.reparaturAndroiden;
	}
	
	/**
	 * Setzt den Broadcastkommunikator.
	 * @param broadcastKommunikator - Neuer Broadcastkommunikator.
	 */
	public void setbroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}
	
	/**
	 * Gibt den Broadcastkommunikator.
	 * @return Der Broadcastkommunikator.
	 */
	public ArrayList<String> getbroadcastkommunikator() {
		return this.broadcastKommunikator;
	}
	
	/**
	 * Setzt das Ladungsverzeichnis.
	 * @param ladungsverzeichnis - Neues Ladungsverzeichnins.
	 */
	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}
	
	/**
	 * Gibt das Ladungsverzeichnis.
	 * @return Das Ladungsverzeichnis.
	 */
	public ArrayList<Ladung> getladungsverzeichnis() {
		return this.ladungsverzeichnis;
	}
	
	/**
	 * Gibt alle Attribute in der Konsole aus.
	 */
	public void zustandAusgeben() {
		System.out.println("schiff " + getSchiff());
		System.out.println("energieversorgung "+ getEnergieversorgung()+ "%");
		System.out.println("schutzschilde "+ getSchutzschilde()+ "%");
		System.out.println("lebenserhaltungssysteme "+ getLebenserhaltungsSysteme()+ "%");
		System.out.println("huelle "+ getHuelle()+ "%");
		System.out.println("photonentorpedos "+ getPhotonentorpedos());
		System.out.println("reparaturAndroiden "+ getReparaturAndroiden());
	}
	
	/**
	 * Gibt das Ladungsverzeichnis in der Konsole aus.
	 */
	public void ladungsverzeichnisAusgeben() {
		for(Ladung temp : this.ladungsverzeichnis) {
			System.out.println("ladungsbezeichnung: "+ temp.getladungsbezeichnung());
			System.out.println("menge: " + temp.getMenge());
		}
	}
	
	/**
	 * F�gt dem Ladungsverzeichninis eine neue Ladung hinzu.
	 * @param ladung - Hinzuzuf�gende Ladung.
	 */
	public void ladungAufnehmen(Ladung ladung) {
		this.ladungsverzeichnis.add(ladung);
	}
	
	/**
	 * Schie�t Photonentorpedos auf Ziel ab und verringert die Anzahl um 1.
	 * @param raumschiff - Zielobjekt.
	 */
	public void photonentorpedoAbschie�en(Raumschiff raumschiff) {
		if(this.getPhotonentorpedos() == 0) {
			nachrichtAnAlle("-=*Click*=-");   
		}else {
			this.photonentorpedos--;
			nachrichtAnAlle("Photonentorpedo Abgeschossen");
			raumschiff.trefferVermerken();
		}
	}
	
	/**
	 * Schie�t die Phaserkanono ab und verringert die Energieversorgung. Verringert Wert um 50 oder gibt Meldung "Click" wieder. 
	 * @param raumschiff - Zielobjekt. 
	 */
	public void phaserkanoneAbschie�en(Raumschiff raumschiff) {
		if (getEnergieversorgung()<50) {
			nachrichtAnAlle("-=*Click*=-");
		}else {
			setEnergieversorgung(getEnergieversorgung()-50);
			nachrichtAnAlle("Phaserkanone Abgeschossen");
			raumschiff.trefferVermerken();
		}
	}
	
	/**
	 * Gibt in der Konsole den Treffer aus. 
	 */
	public void trefferVermerken() {
		System.out.println(getSchiff() +" wurde getroffen!");
	}
	
	/**
	 * Sendet eine Nachricht an alle, in der Konsole. 
	 * @param nachricht - Die neue Nachricht 
	 */
	public void nachrichtAnAlle(String nachricht) {
		System.out.println(nachricht);
	}
}
	
	
	
	
	