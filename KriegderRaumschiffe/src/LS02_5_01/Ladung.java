package LS02_5_01;

/**
 * Blaupause f�r Ladung.
 * @author L.J
 * @version 1.0
 */
public class Ladung {
	private String ladungsbezeichnung;
	private int menge;
	
	/**
	 * Standart Konstruktor - Setzt alles auf leer/0.
	 */
	public Ladung() {
		this.ladungsbezeichnung = "";
		this.menge = 0;
	}
	
	/**
	 * Setzt alle Attribute auf ihre Werte 
	 * @param ladungsbezeichnung - Bezeichnung der Ladung. 
	 * @param menge - Menge der Ladung. 
	 */
	public Ladung(String ladungsbezeichnung, int menge) {
		this.ladungsbezeichnung = ladungsbezeichnung;
		this.menge = menge;
	}
	
	/**
	 * Setzt die Ladungsbezeichnung. 
	 * @param ladungsbezeichnung - Neue Ladungsbezeichnung.
	 */
	public void setladungsbezeichnung(String ladungsbezeichnung) {
		this.ladungsbezeichnung = ladungsbezeichnung;
	}
	
	/**
	 * Gibt die Ladungsbezeichnung.
	 * @return Die Ladungsbezeichnung. 
	 */
	public String getladungsbezeichnung() {
		return this.ladungsbezeichnung;
	}
	
	/**
	 * Setzt die Menge.
	 * @param menge - Die neue Menge.
	 */
	public void setMenge(int menge) {
		this.menge = menge;
	}
	
	/**
	 * Gibt die Menge.
	 * @return Die Menge.
	 */
	public int getMenge() {
		return this.menge; 
		
	}
}